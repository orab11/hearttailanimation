const bodyE1=document.querySelector("body");
bodyE1.addEventListener("mousemove",(e)=>{
      console.log("moved");
      const posX=e.offsetX;//to get the position of the mouse movement in the x-axis
      const posY=e.offsetY;//to get the position of the mouse movement in the Y-axis
      console.log(e.offsetX);
      console.log(e.offsetY);
      const spanE1=document.createElement("span");//create span element
      spanE1.style.left=posX+"px";
      spanE1.style.top=posY+"px";
      const size=Math.random()*100;
      console.log(size);
      spanE1.style.width=size+"px"; //set size(width) for the created span elt
      spanE1.style.height=size+"px"; //set size(Height) for the created span elt
      bodyE1.appendChild(spanE1); //add the span the element as child to the body
      setTimeout(()=>{
            spanE1.remove();
      },2000); //remove the created span elt in every 2s
})